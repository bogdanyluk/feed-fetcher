// eslint-disable-next-line no-extend-native
Array.prototype.map = function (callback, thisArg) {
  const arrLen = this.length;
  const nextArray = new Array(arrLen);

  for (let i = 0; i < arrLen; i += 1) {
    if (i in this) {
      const nextValue = callback.call(thisArg, this[i], i, this);
      nextArray[i] = nextValue;
    }
  }
  return nextArray;
};

// eslint-disable-next-line no-extend-native
Array.prototype.filter = function (callback, thisArg) {
  const nextArray = [];
  const iterator = this[Symbol.iterator]();

  for (let i = 0; ; i += 1) {
    const { value, done } = iterator.next();
    if (i in this) {
      const passedFilter = callback.call(thisArg, value, i, this);
      if (passedFilter) nextArray.push(value);
    }
    if (done) return nextArray;
  }
};

// eslint-disable-next-line no-extend-native
Array.prototype.reduce = function (callback, initialValue) {
  if (!this) {
    throw new TypeError("Reduce is called on null or undefined");
  }

  if (typeof callback !== "function") {
    throw new TypeError(callback + " is not a function");
  }

  const arrLength = this.length;

  if (!Object.keys(this).length && initialValue === undefined) {
    throw new TypeError("Reduce of empty array with no initial value");
  }

  let accumulator = initialValue === undefined ? this[0] : initialValue;

  for (let i = initialValue === undefined ? 1 : 0; i < arrLength; i += 1) {
    const value = this[i];
    if (i in this) {
      accumulator = callback(accumulator, value, i, this);
    }
  }

  return accumulator;
};
