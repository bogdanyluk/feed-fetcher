import { combineReducers } from 'redux';
import { moduleName as appModuleName } from '@/components/FeedContainer/constants';
import appReducers from '@/components/FeedContainer/reducers';

export default combineReducers({
  [appModuleName]: appReducers,
});