import { createStore, applyMiddleware } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import rootEpic from './epics';
import rootReducer from './reducers';

const epicMiddlware = createEpicMiddleware();

export const configureStore = () => {
  const store = createStore(rootReducer, applyMiddleware(epicMiddlware));
  epicMiddlware.run(rootEpic);
  return store;
};
