import { combineEpics } from 'redux-observable';
import appEpics$ from '@/components/FeedContainer/epics';

export default combineEpics(appEpics$);