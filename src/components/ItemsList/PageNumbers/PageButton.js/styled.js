import styled from 'styled-components';

export const PageButton = styled.button`
  :not(:last-child) {
    margin-right: 5px;
  }
  color: ${({ isActive }) => (isActive ? 'red' : 'black')};
`;
