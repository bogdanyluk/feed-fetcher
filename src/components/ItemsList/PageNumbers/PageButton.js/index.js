import React from 'react';
import PropTypes from 'prop-types';
import * as s from './styled';

const PageButton = ({ title, isActive, onClick }) => (
  <s.PageButton isActive={isActive} onClick={onClick}>
    {title}
  </s.PageButton>
);

PageButton.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  isActive: PropTypes.bool,
  onClick: PropTypes.func,
};

PageButton.defaultProps = {
  isActive: false,
}

export default PageButton;
