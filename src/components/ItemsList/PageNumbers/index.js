import React from 'react';
import PropTypes from 'prop-types';
import * as s from './styled';
import PageButton from './PageButton.js';

const PageNumbers = ({ pageNum, pageSize, listSize, onChange }) => {
  const pagesAmount = Math.ceil(listSize / pageSize);

  const buttons = [...Array(pagesAmount)].reduce((acc, item, index) => {
    const isFirstPage = index === 0;
    const isLastPage = index === pagesAmount - 1;
    const distanceToCurrent = index - pageNum;
    const isAtTheBeginning = pageNum < 4 && index < 6;
    const isAtTheEnd = pageNum > pagesAmount - 5 && index > pagesAmount - 7;

    if (
      Math.abs(distanceToCurrent) > 2 &&
      !isAtTheBeginning &&
      !isAtTheEnd &&
      !isFirstPage &&
      !isLastPage
    ) {
      const buttonAlreadyAdded = acc[acc.length - 1].title === '...';
      if (buttonAlreadyAdded) return acc;
      return [
        ...acc,
        {
          title: '...',
          onClick() {
            if (distanceToCurrent > 0) {
              onChange(Math.min(pageNum + 5, pagesAmount - 1));
            } else {
              onChange(Math.max(pageNum - 5, 0));
            }
          },
        },
      ];
    }

    return [
      ...acc,
      {
        title: index + 1,
        isActive: index === pageNum,
        onClick() {
          onChange(index);
        },
      },
    ];
  }, []);

  return (
    <s.PageNumbers>
      {buttons.map(({ title, isActive, onClick }, index) => (
        <PageButton
          key={`${title}${index}`}
          isActive={isActive}
          title={title}
          onClick={onClick}
        />
      ))}
    </s.PageNumbers>
  );
};

PageNumbers.propTypes = {
  pageNum: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  listSize: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default PageNumbers;
