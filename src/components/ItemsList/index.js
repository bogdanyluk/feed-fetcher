import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import * as s from './styled';
import PageNumbers from './PageNumbers';

const ItemsList = ({ items, itemKey, RenderItemComponent }) => {
  const [pageNum, setPageNum] = useState(0);

  useEffect(() => {
    setPageNum(0);
  }, [items]);

  const pageSize = 5;

  const startIndex = pageNum * pageSize;
  const endIndex = startIndex + pageSize;

  return (
    <Fragment>
      <PageNumbers
        pageSize={pageSize}
        pageNum={pageNum}
        listSize={items.length}
        onChange={setPageNum}
      />
      <s.ItemsList>
        {items.slice(startIndex, endIndex).map((item) => (
          <RenderItemComponent key={item[itemKey]} {...item} />
        ))}
      </s.ItemsList>
    </Fragment>
  );
};

ItemsList.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
  itemKey: PropTypes.string.isRequired,
  RenderItemComponent: PropTypes.func.isRequired,
};

export default ItemsList;
