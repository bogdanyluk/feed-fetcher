import { FETCH_RSS_REQUESTED, fetchRssSucceeded, fetchRssFailed } from './actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { combineEpics } from 'redux-observable';
import * as apiService from '@/services/apiService';
import { of } from 'rxjs';

const fetchRss$ = (action$) =>
  action$.ofType(FETCH_RSS_REQUESTED).pipe(
    switchMap(({ url }) =>
      apiService.request$(url).pipe(
        map((string) =>
          new window.DOMParser().parseFromString(string, 'text/xml')
        ),
        map((data) => {
          const title = data.querySelector('title').innerHTML;
          const description = data.querySelector('description').innerHTML;
          const items = Array.from(data.querySelectorAll('item'), (item) => {
            const childNodes = item.childNodes;
            return Array.from(childNodes).reduce(
              (acc, child) => ({
                ...acc,
                [child.tagName]: child.textContent,
              }),
              {}
            );
          });

          return fetchRssSucceeded({
            title,
            description,
            items,
          });
        }),
        catchError(error => of(fetchRssFailed(error.message)))
      )
    )
  );

export default combineEpics(fetchRss$);
