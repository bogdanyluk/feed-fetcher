import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Title from './Title';
import * as s from './styled';

const FeedItem = ({ title, link, description }) => {
  const [showDescription, setShowDescripiton] = useState(false);
  function getDescriptionHTML() {
    // i know that this needs to be sanitized :)
    return {__html: description}
  }

  function toggleDescription() {
    setShowDescripiton(!showDescription);
  }

  return (
  <s.FeedItem>
    <Title title={title} link={link} />
    <s.DescriptionButton onClick={toggleDescription}>{`${showDescription ? 'Hide' : 'Show'} Description`}</s.DescriptionButton>
    {showDescription && (
      <div dangerouslySetInnerHTML={getDescriptionHTML()} />
    )}
  </s.FeedItem>
)};

FeedItem.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default FeedItem;
