import styled from 'styled-components';

export const Title = styled.h3`
  margin-top: 0;
`;

export const Link = styled.a.attrs(({ link }) => ({
  href: link,
}))``;