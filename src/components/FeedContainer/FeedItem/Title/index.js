import React from 'react';
import PropTypes from 'prop-types';
import * as s from './styled';

const Title = ({ title, link }) => (
  <s.Title>
    <s.Link link={link}>{title}</s.Link>
  </s.Title>
);

Title.propTypes = {
  title: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};

export default Title;
