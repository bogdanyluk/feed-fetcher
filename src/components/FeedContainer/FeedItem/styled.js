import styled from 'styled-components';

export const FeedItem = styled.article`
  margin: 5px;
  border: 1px solid gray;
  padding: 5px;
`;

export const Description = styled.div``;

export const DescriptionButton = styled.button``;