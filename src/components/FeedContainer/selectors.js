import { createSelector } from 'reselect';
import { moduleName } from './constants';

const moduleSel = (state) => state[moduleName];

export const feedSel = createSelector(moduleSel, ({ feed }) => feed);
export const isLoadingSel = createSelector(moduleSel, ({ isLoading }) => isLoading);
export const errorMessageSel = createSelector(moduleSel, ({ errorMessage }) => errorMessage);
