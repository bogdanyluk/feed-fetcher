import { moduleName } from './constants';

export const FETCH_RSS_REQUESTED = `${moduleName}/FETCH_RSS_REQUESTED`;
export const FETCH_RSS_SUCCEEDED = `${moduleName}/FETCH_RSS_SUCCEEDED`;
export const FETCH_RSS_FAILED = `${moduleName}/FETCH_RSS_FAILED`;

export const fetcRssRequested = url => ({
  type: FETCH_RSS_REQUESTED,
  url,
});

export const fetchRssSucceeded = feed => ({
  type: FETCH_RSS_SUCCEEDED,
  feed,
})

export const fetchRssFailed = errorMessage => ({
  type: FETCH_RSS_FAILED,
  errorMessage,
})
