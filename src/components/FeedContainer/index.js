import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { feedSel } from './selectors';
import FeedItem from './FeedItem';
import * as s from './styled';
import ItemsList from '@/components/ItemsList';

const FeedContainer = () => {
  const feed = useSelector(feedSel);

  if (!feed) return null;

  const { title, description, items } = feed;
  return (
    <s.Container>
      <s.Title>{title}</s.Title>
      <s.Description>{description}</s.Description>
      {!!items && (
        <ItemsList
          RenderItemComponent={FeedItem}
          items={items}
          itemKey="link"
        />
      )}
    </s.Container>
  );
};

FeedContainer.propTypes = {
  feed: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    items: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string,
        link: PropTypes.string,
        description: PropTypes.string,
      })
    ),
  }),
};

export default FeedContainer;
