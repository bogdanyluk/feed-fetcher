import produce from 'immer';
import { FETCH_RSS_SUCCEEDED, FETCH_RSS_FAILED, FETCH_RSS_REQUESTED } from './actions';

export default produce((draft, action) => {
  // eslint-disable-next-line default-case
  switch (action.type) {
    case FETCH_RSS_REQUESTED:
      draft.isLoading = true;
      draft.errorMessage = null;
      break;
    case FETCH_RSS_SUCCEEDED:
      draft.feed = action.feed;
      draft.isLoading = false;
      break;
    case FETCH_RSS_FAILED:
      draft.feed = {};
      draft.isLoading = false;
      draft.errorMessage = action.errorMessage
  }
}, {
  feed: {},
  isLoading: false,
})

