import styled from 'styled-components';

export const FetchFeedButton = styled.button`
  width: 100px;
  margin-left: 5px;
  text-align: start;
`;
