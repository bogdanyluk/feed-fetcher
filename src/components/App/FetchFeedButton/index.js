import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import * as s from './styled';

const FetchFeedButton = ({ inProgress, onClick }) => {
  const [dots, setDots] = useState('');

  function updateDots() {
    if (dots.length >= 3) {
      return setDots('');
    }
    return setDots(`${dots}.`);
  }

  useEffect(() => {
    setTimeout(updateDots, 200);
  }, [inProgress, dots]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <s.FetchFeedButton onClick={onClick}>{`FETCH${
      inProgress ? `ING${dots}` : ''
    }`}</s.FetchFeedButton>
  );
};

FetchFeedButton.propTypes = {
  inProgress: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FetchFeedButton;
