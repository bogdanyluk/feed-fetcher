import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetcRssRequested } from '../FeedContainer/actions';
import { isLoadingSel, errorMessageSel } from '../FeedContainer/selectors';
import FeedContainer from '@/components/FeedContainer';
import FetchFeedButton from './FetchFeedButton';
import * as s from './styled';

function App() {
  const dispatch = useDispatch();
  const fetchFeedError = useSelector(errorMessageSel);
  const isLoadingFeed = useSelector(isLoadingSel);
  const [url, setUrl] = useState('');

  function handleFetchRSS() {
    if (url) {
      dispatch(fetcRssRequested(url))
    }
  }

  function handleUrlChange(e) {
    setUrl(e.target.value);
  }

  return (
    <div>
      <input value={url} onChange={handleUrlChange} />
      <FetchFeedButton inProgress={isLoadingFeed} onClick={handleFetchRSS} />
      {fetchFeedError && (
        <s.ErrorMessage>{fetchFeedError}</s.ErrorMessage>
      )}
      <FeedContainer />
    </div>
  );
}

export default App;
