import { fromFetch } from 'rxjs/fetch';

function addProtocol(url) {
  const hasProtocol = /^http(?:s)?:\/\/.*$/.test(url);
  return `${hasProtocol ? '' : 'https://'}${url}`;
}

export const request$ = url => {
  const urlWithProtocol = addProtocol(url);
  return fromFetch(urlWithProtocol, {
    selector: (response) => response.text(),
  });
};
